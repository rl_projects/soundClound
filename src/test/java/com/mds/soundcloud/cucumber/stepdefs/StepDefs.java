package com.mds.soundcloud.cucumber.stepdefs;

import com.mds.soundcloud.SoundCloudServerApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = SoundCloudServerApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
