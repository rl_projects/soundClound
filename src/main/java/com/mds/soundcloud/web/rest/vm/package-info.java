/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mds.soundcloud.web.rest.vm;
